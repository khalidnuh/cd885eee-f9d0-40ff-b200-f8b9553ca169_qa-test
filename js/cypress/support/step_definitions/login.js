const {Given, When, And, Then } = require('cypress-cucumber-preprocessor/steps');

window.email='test@drugdev.com';
window.password='supers3cret';



beforeEach(function(){
  cy.clearCookies()
  cy.visit('http://sprinkle-burn.glitch.me')
})

Given("the user has the correct credentials",() => {
   
   expect(email).to.equal('test@drugdev.com')
   expect(password).to.equal('supers3cret')
});
Given("the user has the incorrect credentials",() => {
    email = "incorrect@drugdev.com"
    password= "wrongpassword"
});
When("the user enters username", () => {
  cy.get("input[name='email']").type(email)

});

And("the user enters password", () => {
  cy.get("input[name='password']").type(password)
});

And("clicks Login", () => {
  cy.get(".f5[aria-label='login']").click()
});

Then("the user is presented with a welcome message", () => {
  
  cy.contains('Welcome')
});

Then("the user is presented with a error message", () => {
  expect(cy.get('#login-error-box'),true)
 });
 

